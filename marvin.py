#!/usr/bin/env python3

# --------------------------------------------------------------------------------------------------
# Title           : marvin.py.py
# 
# Description     : 
# Author          : James Dooley
# Date            : 27.11.19
# Version         : 0.0.1
# Usage           : python3 marvin.py.py
# Notes           :
# Python Version  : 3.7.0
#
# --------------------------------------------------------------------------------------------------

"""marvin.py.py: """

__author__ = "James Dooley"
__credits__ = []
__license__ = "MIT"
__version__ = "1.0.1"
__maintainer__ = "James Dooley"
__email__ = "jdooley.ch@gmail.com"
__status__ = "Prototype"

import math

# --------------------------------------------------------------------------------------------------
# This function displays the menu
#
def display_menu():
    """
    Display the application window
    """
    print("\n\t*** M A R V I N - " + __version__ + " ***\n")
    print("\tMenu:")
    print("\t\t1. Add two numbers")
    print("\t\t2. Add three numbers")
    print("\t\t3. Subtract numbers")
    print("\t\t4. Calculate simple interest")
    print("\t\t5. Calculate Averages")
    print("\t\t6. Calculate Shopping Discount")
    print("\n\t\t0. Exit")


# --------------------------------------------------------------------------------------------------
# Addition two numbers
#
def add_two_numbers():
    """
    Add two numbers
    """
    print("\nOK " + user_name + ", let's do some addition of two numbers....")

    while True:
        print("\n")
        num1 = input("Enter the first number => ")
        if not num1:
            num1 = 0
        num2 = input("Enter the second number => ")
        if not num2:
            num2 = 0
        sum_of_numbers = int(num1) + int(num2)
        if sum == 0:
            print("\nOK, we're done with addition of two numbers.")
            break
        print("The answer is: " + str(sum_of_numbers))


# --------------------------------------------------------------------------------------------------
# Addition three numbers
#
def add_three_numbers():
    """
    Add three numbers
    """
    print("\nOK " + user_name + ", let's do some addition of three numbers....")

    while True:
        print("\n")
        num1 = input("Enter the first number => ")
        if not num1:
            num1 = 0
        num2 = input("Enter the second number => ")
        if not num2:
            num2 = 0
        num3 = input("Enter the third number => ")
        if not num3:
            num3 = 0
        sum_of_numbers = int(num1) + int(num2) + int(num3)
        if sum == 0:
            print("\nOK, we're done with addition of three numbers.")
            break
        print("The answer is: " + str(sum_of_numbers))


# --------------------------------------------------------------------------------------------------
# Subtract numbers
#
def subtract_numbers():
    """
    Subtract numbers
    """
    print("\nOK " + user_name + ", let's do some subtraction....")

    while True:
        print("\n")
        num1 = input("Enter the first number => ")
        if not num1:
            num1 = 0
        num2 = input("Enter the second number => ")
        if not num2:
            num2 = 0
        sum_of_numbers = int(num1) - int(num2)
        if sum == 0:
            print("\nOK, we're done with addition of three numbers.")
            break
        print("The answer is: " + str(sum_of_numbers))


# --------------------------------------------------------------------------------------------------
# Simple interest
#
def calculate_interest():
    """
    Perform a simple interest calculation
    """
    print("\nOK " + user_name + ", let's do some simple interest calculations....")

    while True:
        print("\n")
        num1 = input("Enter the Principal ($) => ")
        if not num1:
            num1 = 0
        num2 = input("Enter the Rate (%) => ")
        if not num2:
            num2 = 0
        num3 = input("Enter the Time => ")
        if not num3:
            num3 = 0

        p = int(num1)
        r = float(num2) / 100
        t = int(num3)

        if (p == 0) or (r == 0) or (t == 0):
            print("\nOK, we're done with interest calculations.")
            break

        amount = p * (1 + (r * t))
        print("The answer is: " + str(amount))


# --------------------------------------------------------------------------------------------------
# Simple interest
#
def calculate_averages():
    """
    Calculates the average of a number of numbers
    """
    print("\nOK " + user_name + ", let's do some average calculations....\n")

    items = []

    while True:
        item = input("Enter a number: ")

        if not item:
            break

        items.append(item)

    if len(items) > 0:
        count = len(items)
        total = 0

        for item in items:
            total = total + int(item)

        ave = total / count

        print("The average of the " + str(count) + " numbers you entered is: " + str(ave) + ".")


# --------------------------------------------------------------------------------------------------
# Round a number up
#
def round_up(n, decimals=0):
    """
    Rounds the given number to a given number of decimal places
    """
    multiplier = 10 ** decimals
    return math.ceil(n * multiplier) / multiplier


# --------------------------------------------------------------------------------------------------
# Calculates the discount for a shopping list
#
def calculate_shopping_discount():
    """
    Calculates the discount on a list of items purchased
    """
    shopping_list = []
    shopping_amounts = []

    print("\nOK " + user_name + ", let's see how much discount you got....\n")

    # Get the list of items
    while True:
        item = input("\tEnter the item name: ")

        if not item:
            break

        shopping_list.append(item)

    # Get the prices
    print("\n")
    while True:
        for item in shopping_list:
            price = None

            while not price:
                price = input("\tWe need the price for " + item + ": ")

                if price:
                    break

            shopping_amounts.append(price)
        break

    # Do the calculation
    total = 0
    for item in shopping_amounts:
        total = total + int(item)

    discount_rate = None

    print("\n")
    while not discount_rate:
        discount_rate = input("\tEnter the discount % as an integer: ")

    discount = round_up(total * (int(discount_rate) / 100), 2)
    net_price = total - discount

    print("\n\tThe net price is : " + str(net_price) + " and you received a discount of: " + str(discount))


# --------------------------------------------------------------------------------------------------
# Program Entry Point
#
user_name = input("What is your name? ")

if not user_name:
    user_name = "Guest"

while True:
    display_menu()
    userChoice = input("\n\tWhat do you want to do: ")

    if not userChoice:
        userChoice = -1
    choice = int(userChoice)

    if choice == 0:
        break
    elif choice == 1:
        add_two_numbers()
    elif choice == 2:
        add_three_numbers()
    elif choice == 3:
        subtract_numbers()
    elif choice == 4:
        calculate_interest()
    elif choice == 5:
        calculate_averages()
    else:
        calculate_shopping_discount()

print("\n\tOk, we're done, bye bye...")
